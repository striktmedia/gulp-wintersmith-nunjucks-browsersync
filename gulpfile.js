var gulp = require('gulp'),
	gutil = require('gulp-util'),
	sass = require('gulp-sass'),
	browserSync = require('browser-sync'),
	autoprefixer = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify'),
	jshint = require('gulp-jshint'),
	header = require('gulp-header'),
	rename = require('gulp-rename'),
	minifyCSS = require('gulp-minify-css'),
	runWintersmith = require('run-wintersmith'),
	del = require('del'),
	runSequence = require('run-sequence'),
	spawn = require('child_process').spawn,
	package = require('./package.json');


gulp.slurped = false; // step 1

var banner = [
	'/*!\n' +
	' * <%= package.name %>\n' +
	' * <%= package.title %>\n' +
	' * <%= package.url %>\n' +
	' * @author <%= package.author %>\n' +
	' * @version <%= package.version %>\n' +
	' * Copyright ' + new Date().getFullYear() + '. <%= package.license %> licensed.\n' +
	' */',
	'\n'
].join('');


//
//	Config Directories
//
var BUILD_DIR = 'build',
	ASSETS_DIR = 'resources',
	CONTENT_DIR = 'sources/contents',
	TEMPLATES_DIR = 'sources/templates',
	RESOURCES_DIR = 'resources',
	RESOURCES_SCSS_DIR = 'resources/scss',
	RESOURCES_JS_DIR = 'resources/js';




//
//	Clean task - Cleans everything in build dir
//
gulp.task('clean', function (cb) {
	del(['build'], cb);
});


//
//	CSS task - Compile SASS to CSS and minify the output to a new file.
//	Inject new css file in the browser
//
gulp.task('scss', function () {
	return gulp.src(RESOURCES_SCSS_DIR + '/main.scss')
		.pipe(sass({errLogToConsole: true}))
		.pipe(autoprefixer('last 4 version'))
		.pipe(gulp.dest(BUILD_DIR + '/' + ASSETS_DIR + '/css'))
		.pipe(minifyCSS())
		.pipe(rename({ suffix: '.min' }))
		.pipe(header(banner, { package : package }))
		.pipe(gulp.dest(BUILD_DIR + '/' + ASSETS_DIR + '/css'))
		.pipe(browserSync.reload({stream:true, once: true}));
});


//
//	JS task - Check JavaScript on errors and minify the output to a new file
//	Inject new js file in the browser
//
gulp.task('js', function () {
	return gulp.src(RESOURCES_JS_DIR + '/scripts.js')
		.pipe(jshint('.jshintrc'))
		.pipe(jshint.reporter('default'))
		.pipe(header(banner, { package : package }))
		.pipe(gulp.dest(BUILD_DIR + '/' + ASSETS_DIR + '/js'))
		.pipe(uglify())
		.pipe(header(banner, { package : package }))
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulp.dest(BUILD_DIR + '/' + ASSETS_DIR + '/js'))
		.pipe(browserSync.reload({stream:true, once: true}));
});


//
//	BrowserSync task - Set up BrowserSync
//
gulp.task('browser-sync', function() {
	browserSync.init(null, {
		server: {
			baseDir: BUILD_DIR
		}
	});
});


//
//	BrowserSync reload task - Refresh the browser
//
gulp.task('bs-reload', function () {
	gutil.log('BrowserSync Reloaded');
	browserSync.reload();
});


//
//	Wintersmith Build task
//
gulp.task('ws-build', function (callback) {
	// Tell Wintersmith to build
	runWintersmith.build(function(){
		// Log on successful build
		gutil.log('Wintersmith has finished building!');
		browserSync.reload();
		// Tell gulp task has finished
		callback();
	});
});


//
//	Wintersmith Preview task
//
gulp.task('ws-preview', function () {
	// Tell Wintersmith to run in preview mode
	runWintersmith.preview();
});


//
//	Build task
//
gulp.task('build', function (callback) {
	runSequence(
		'clean',
		'ws-build',
		['scss', 'js'],
		callback);
});


//
//	Watch sources and resources files
//
gulp.task('watch', function () {
	function reportChange(e) {
		gutil.log(gutil.template('File <%= file %> was <%= type %>, rebuilding...', {
			file: gutil.colors.cyan(e.path),
			type: e.type
		}));
	}

	if (!gulp.slurped) {

		gulp.watch(RESOURCES_SCSS_DIR + "/**/*.scss", ['scss']);
		gulp.watch(RESOURCES_JS_DIR + "/**/*.js", ['js']);

		// Watch Jade template files
		gulp.watch(TEMPLATES_DIR + '/**/*', ['ws-build'])
			.on('change', reportChange);

		// Watch Markdown files
		gulp.watch(CONTENT_DIR + '/**/*', ['ws-build'])
			.on('change', reportChange);

		gulp.watch("gulpfile.js", ['reload']);

		gulp.slurped = true; // step 3

	}

});


//
// Default Gulp task
//
gulp.task('reload', ['build', 'watch']);
gulp.task('default', ['build', 'browser-sync', 'watch']);

